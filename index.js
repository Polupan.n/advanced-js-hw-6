`use strict`;
// ## Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Асинхронність в JavaScript означає виконання коду, який може виконуватися паралельно з іншим кодом, не очікуючи завершення попереднього. У звичайному синхронному коді кожна операція виконується послідовно, одна за одною, і програма блокується, поки не завершиться попередня операція.

// Асинхронні функції у JavaScript дозволяють виконувати операції, які можуть займати час, такі як завантаження файлів або відправка запитів до сервера, без блокування інших операцій у програмі. Вони виконуються в фоновому режимі, і програма може продовжувати виконувати інші операції, не чекаючи завершення асинхронної операції.

// Це досягається за допомогою концепцій зворотного виклику (callback), обіцянок (promises) та асинхронних функцій (async/await), які дозволяють структурувати код так, щоб операції могли виконуватися асинхронно, а результати оброблялися після завершення цих операцій.

// ## Завдання
// Написати програму "Я тебе знайду по IP"

// #### Технічні вимоги:
// - Створити просту HTML-сторінку з кнопкою `Знайти по IP`.
// - Натиснувши кнопку - надіслати AJAX запит за адресою `https://api.ipify.org/?format=json`, отримати звідти IP адресу клієнта.
// - Дізнавшись IP адресу, надіслати запит на сервіс `https://ip-api.com/` та отримати інформацію про фізичну адресу.
// - під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// - Усі запити на сервер необхідно виконати за допомогою async await.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Литература:
// - [Async/await](https://learn.javascript.ru/async-await)
// - [async function](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/async_function)
// - [Документація сервісу ip-api.com](http://ip-api.com/docs/api:json)

const btn = document.querySelector(`#btn`);
const getIp = `https://api.ipify.org/?format=json`;
const getIpInfo = `http://ip-api.com/json/`;
const root = document.querySelector(`#root`);

class Request {
  async getInfo() {
    const responseIp = await fetch(getIp);
    const clientIp = await responseIp.json();
    const responseInfo = await fetch(`${getIpInfo}${clientIp.ip}`);
    return responseInfo.json();
  }
}

class Render {
  render(obj) {
    const { timezone, country, region, city } = obj;
    const timezoneElement = document.createElement("p");
    timezoneElement.textContent = `Timezone: ${timezone}`;
    const countryElement = document.createElement("p");
    countryElement.textContent = `Country: ${country}`;
    const regionElement = document.createElement("p");
    regionElement.textContent = `Region: ${region}`;
    const cityElement = document.createElement("p");
    cityElement.textContent = `City: ${city}`;
    const container = document.createElement("div");
    container.append(
      timezoneElement,
      countryElement,
      regionElement,
      cityElement
    );

    return container;
  }
}
const render = new Render();
const request = new Request();
btn.addEventListener(`click`, () => {
  request
    .getInfo()
    .then((data) => {
      root.innerHTML = "";
      root.append(render.render(data));
    })
    .catch((error) => {
      console.error(error);
    });
});
